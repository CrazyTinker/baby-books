# Baby's First Book Of Robots
Just a little book for my daughter on robots

## Setup
make config 

## Building
make debug

## Installing
make install
OUTPUT = platforms/android/build/outputs/apk/

DEBUG = $(OUTPUT)/android-debug.apk

RELEASE = $(OUTPUT)/android-release-unsigned.apk

APK = tk.crazytinker.babyrobotbook.apk

clean : $(DEBUG) $(RELEASE)
		rm $(OUTPUT)/*

config :
		cordova platform add android
		rm -rf platforms/android/res/
		cp -R res platforms/android/

debug :
		cordova build android --debug
		ln -sf $(DEBUG) $(APK)

release :
		cordova build android --release
		ln -sf $(RELEASE) $(APK)

install : $(APK)
		adb install $(APK)
